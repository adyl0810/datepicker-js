// ****************** Задание 1 ********************

const body = $('body')
const btn = $('button')
btn.text("Click for modal")
btn.on('click', () => {
    createCustomAlert("Hello", "Hello Hello")
})

function createCustomAlert(title, text) {
    const darkLayer = $('<div id="shadow">')
    const modal = $('<div class="custom-alert">')
    const modalTitle = $('<h1 class="title">').text(title)
    const modalText = $('<div class="text">').text(text)
    const modalButton = $('<button class="btn btn-primary">').text("Close")
    modal.append(modalTitle)
    modal.append(modalText)
    modal.append(modalButton)
    modal.css({
        display : "block"
    })
    body.append(darkLayer)
    body.append(modal)

    modalButton.on('click', () => {
        darkLayer.remove()
        modal.css({
            display: "none"
        })
    })

    darkLayer.on('click', () => {
        darkLayer.remove()
        modal.css({
            display: "none"
        })
    })
}

// ****************** Задание 2 ********************

const tabs = $('#tabs')

function createTabs(container) {
    const tab = container.children('div').children()
    const tabLink = container.children('ul').children().children()
    tab.hide().filter(':first').show();

    tabLink.click(function(){
        tab.hide();
        tab.filter(this.hash).show();
        tabLink.removeClass('active');
        $(this).addClass('active');
        return false;
    }).filter(':first').click();

}

createTabs(tabs)

// ****************** Задание 3 ********************

$( function() {
    $( "#datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true
    });
} );

